package br.com.parus.model.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Builder
@Getter
@Setter
public class CreateUserResponse implements Serializable {

    private String id;
    private String userName;

}
