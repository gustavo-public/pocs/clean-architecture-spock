package br.com.parus.api.user;

import br.com.parus.exception.UseCaseException;
import br.com.parus.model.request.CreateUserRequest;
import br.com.parus.model.response.CreateUserResponse;
import reactor.core.publisher.Mono;


public interface CreateUserApi {

    Mono<CreateUserResponse> create(CreateUserRequest request) throws UseCaseException;

}
