package br.com.parus.model.request;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Setter
@Getter
public class CreateUserRequest {

    private String userName;
    private String password;

}
