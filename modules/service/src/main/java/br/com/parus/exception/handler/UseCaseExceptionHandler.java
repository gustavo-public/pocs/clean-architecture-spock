package br.com.parus.exception.handler;

import br.com.parus.exception.UseCaseException;
import org.springframework.web.bind.annotation.ExceptionHandler;

public class UseCaseExceptionHandler {

    @ExceptionHandler(value = UseCaseException.class)
    public ErroResponse handle(UseCaseException exception){

        return ErroResponse.builder().code(exception.getCODE())
                                    .message(exception.getMessage())
                                    .build();

    }
}
