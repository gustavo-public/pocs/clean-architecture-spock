package br.com.parus.translator.user;

import br.com.parus.User;
import br.com.parus.gateway.database.entity.UserDataBase;
import br.com.parus.translator.Translator;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class UserDataBaseToUserTranslator implements Translator<UserDataBase, User> {

    @Override
    public User translate(UserDataBase userDataBase) {
        return new ModelMapper().map(userDataBase,User.class);
    }
}
