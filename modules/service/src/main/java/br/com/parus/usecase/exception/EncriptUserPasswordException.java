package br.com.parus.usecase.exception;

import br.com.parus.exception.UseCaseException;


public class EncriptUserPasswordException extends UseCaseException {

    private final static String MESSAGE = "Fail encript user password";

    private final static String CODE = "70-02";

    public EncriptUserPasswordException(Throwable ex){
        this(MESSAGE,ex);
    }

    public EncriptUserPasswordException(String message,Throwable ex){
        this(message,CODE,ex);
    }

    public EncriptUserPasswordException(String message,String code,Throwable ex){
        super(message,code,ex);
    }

}
