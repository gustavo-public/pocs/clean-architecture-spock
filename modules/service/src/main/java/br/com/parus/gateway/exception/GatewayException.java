package br.com.parus.gateway.exception;

public class GatewayException extends Exception {

    public GatewayException(String message, Throwable cause) {
        super(message, cause);
    }
}
