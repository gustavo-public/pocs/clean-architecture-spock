package br.com.parus.exception.handler;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Setter
@Getter
public class ErroResponse {

    private String code;
    private String message;

}
