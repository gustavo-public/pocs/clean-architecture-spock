package br.com.parus.translator.user;

import br.com.parus.User;
import br.com.parus.model.response.CreateUserResponse;
import br.com.parus.translator.Translator;
import org.springframework.stereotype.Component;

@Component
public class UserToCreateUserResponseTranslator implements Translator<User, CreateUserResponse> {

    @Override
    public CreateUserResponse translate(User user) {
        return CreateUserResponse
                    .builder()
                    .id(user.getId())
                    .userName(user.getUserName())
                    .build();
    }
}
