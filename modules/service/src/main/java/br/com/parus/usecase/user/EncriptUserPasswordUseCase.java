package br.com.parus.usecase.user;

import br.com.parus.User;
import br.com.parus.usecase.EncriptSringUseCase;
import br.com.parus.usecase.UseCase;
import br.com.parus.usecase.exception.EncriptStringException;
import br.com.parus.usecase.exception.EncriptUserPasswordException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
@Service
public class EncriptUserPasswordUseCase implements UseCase<User> {

    private final EncriptSringUseCase encriptSringUseCase;

    @Override
    public Mono<User> execute(User user) {

        return encriptSringUseCase
                    .execute(user.getPassword())
                    .map(s -> setPassword(s,user))
                    .onErrorMap(EncriptUserPasswordException::new).log();
    }

    private User setPassword(String encriptedPassword,User user){
        user.setPassword(encriptedPassword);
        return user;
    }

}
