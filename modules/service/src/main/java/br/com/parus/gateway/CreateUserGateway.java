package br.com.parus.gateway;


import br.com.parus.gateway.database.entity.UserDataBase;
import br.com.parus.gateway.exception.CreateUserGatewayException;
import reactor.core.publisher.Mono;


public interface CreateUserGateway  {

    Mono<UserDataBase> create(UserDataBase user) ;

}
