package br.com.parus.gateway.exception;

public class CreateUserGatewayException extends GatewayException {

    public CreateUserGatewayException(String message, Throwable ex) {
        super(message,ex);
    }

}
