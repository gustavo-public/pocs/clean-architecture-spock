package br.com.parus.translator.user;


import br.com.parus.User;
import br.com.parus.gateway.database.entity.UserDataBase;
import br.com.parus.translator.Translator;
import org.springframework.stereotype.Component;

@Component
public class UserToUserDataBaseTranslador implements Translator<User, UserDataBase> {

    @Override
    public UserDataBase translate(User user) {
        return UserDataBase.builder()
                .id(user.getId())
                .userName(user.getUserName())
                .password(user.getPassword())
                .build();
    }

}
