package br.com.parus.exception;

public class CreateUserException extends UseCaseException {

    private final static String MESSAGE = "Fail to create user";

    private final static String CODE = "10-01";

    public CreateUserException(Throwable ex){
        this(MESSAGE,ex);
    }

    public CreateUserException(String message,Throwable ex){
        this(message,CODE,ex);
    }

    public CreateUserException(String message,String code,Throwable ex){
        super(message,code,ex);
    }

}
