package br.com.parus.controller;

import br.com.parus.api.user.CreateUserApi;
import br.com.parus.model.request.CreateUserRequest;
import br.com.parus.model.response.CreateUserResponse;
import br.com.parus.exception.CreateUserException;
import br.com.parus.translator.user.CreateUserRequestToUserTranslator;
import br.com.parus.translator.user.UserToCreateUserResponseTranslator;
import br.com.parus.usecase.user.CreateUserUseCase;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(value = "/user")
@AllArgsConstructor
public class CreateUserController implements CreateUserApi {

    private final CreateUserRequestToUserTranslator createUserRequestToUserTranslator;
    private final UserToCreateUserResponseTranslator userToCreateUserResponseTranslator;
    private final CreateUserUseCase createUserUseCase;

    @Override
    @ResponseStatus(CREATED)
    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public Mono<CreateUserResponse> create(@RequestBody CreateUserRequest request)  {

        return Mono.just(request)
                .map(createUserRequestToUserTranslator::translate)
                .flatMap(createUserUseCase::execute)
                .onErrorMap(CreateUserException::new)
                .log()
                .map(userToCreateUserResponseTranslator::translate);

    }
}
