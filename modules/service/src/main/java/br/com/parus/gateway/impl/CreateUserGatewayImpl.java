package br.com.parus.gateway.impl;

import br.com.parus.gateway.CreateUserGateway;
import br.com.parus.gateway.database.entity.UserDataBase;
import br.com.parus.gateway.database.repository.UserRepository;
import br.com.parus.gateway.exception.CreateUserGatewayException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

import static reactor.core.publisher.Mono.error;

@RequiredArgsConstructor
@Service
public class CreateUserGatewayImpl implements CreateUserGateway {

    private final UserRepository repository;

    @Override
    public Mono<UserDataBase> create(UserDataBase user)  {

        user.setCreatedAt(LocalDate.now());

        return repository.save(user)
                        .onErrorMap(ex -> new CreateUserGatewayException("Erro at saving user",ex));

    }

}
