package br.com.parus.translator;

import org.springframework.stereotype.Component;


public interface Translator<S,D> {

    D translate(S s);
}
