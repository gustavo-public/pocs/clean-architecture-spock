package br.com.parus.gateway.database.repository;

import br.com.parus.gateway.database.entity.UserDataBase;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface UserRepository  extends ReactiveMongoRepository<UserDataBase, Long> {
}
