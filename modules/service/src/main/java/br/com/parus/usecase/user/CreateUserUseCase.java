package br.com.parus.usecase.user;

import br.com.parus.User;
import br.com.parus.exception.CreateUserException;
import br.com.parus.gateway.CreateUserGateway;
import br.com.parus.gateway.exception.CreateUserGatewayException;
import br.com.parus.gateway.exception.GatewayException;
import br.com.parus.translator.user.UserDataBaseToUserTranslator;
import br.com.parus.translator.user.UserToUserDataBaseTranslador;
import br.com.parus.usecase.UseCase;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.UUID;

@AllArgsConstructor
@Service
public class CreateUserUseCase implements UseCase<User> {

    private final EncriptUserPasswordUseCase encriptUserPassWordUseCase;

    private final UserToUserDataBaseTranslador userToUserDataBaseTranslador;
    private final UserDataBaseToUserTranslator userDataBaseToUserTranslator;

    private final CreateUserGateway createUserGateway;

    @Override
    public Mono<User> execute(User user)   {

        return Mono.just(user)
                .flatMap(encriptUserPassWordUseCase::execute)
                .onErrorMap(CreateUserException::new).log()
                .map(userToUserDataBaseTranslador::translate)
                .doOnNext(userDataBase ->  userDataBase.setId(UUID.randomUUID().toString()))
                .flatMap(createUserGateway::create)
                .map(userDataBaseToUserTranslator::translate)
                .onErrorMap(CreateUserException::new).log();

    }


}
