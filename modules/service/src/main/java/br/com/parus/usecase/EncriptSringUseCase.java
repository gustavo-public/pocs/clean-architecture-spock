package br.com.parus.usecase;

import br.com.parus.usecase.exception.EncriptStringException;
import br.com.parus.usecase.exception.NoSuchTextException;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.Base64;

import static org.springframework.util.StringUtils.isEmpty;
import static reactor.core.publisher.Mono.error;
import static reactor.core.publisher.Mono.just;

@Service
public class EncriptSringUseCase implements UseCase<String> {

    private final Base64.Encoder ENCODER = Base64.getEncoder();

    @Override
    public Mono<String> execute(String text)  {

        return just(text)
                .flatMap(s -> isEmpty(s) ? error(new NoSuchTextException(new IllegalArgumentException())) : just(s))
                .map(String::getBytes)
                .map(ENCODER::encodeToString)
                .onErrorMap(EncriptStringException::new).log();
    }
}
