package br.com.parus.usecase.exception;

import br.com.parus.exception.UseCaseException;

public class EncriptStringException extends UseCaseException {

    private final static String MESSAGE = "Fail encript text";

    private final static String CODE = "70-01";

    public EncriptStringException(Throwable ex){
        this(MESSAGE,ex);
    }

    public EncriptStringException(String message,Throwable ex){
        this(message,CODE,ex);
    }

    public EncriptStringException(String message,String code,Throwable ex){
        super(message,code,ex);
    }

}
