package br.com.parus.translator.user;

import br.com.parus.User;
import br.com.parus.model.request.CreateUserRequest;
import br.com.parus.translator.Translator;
import org.springframework.stereotype.Component;

@Component
public class CreateUserRequestToUserTranslator implements Translator<CreateUserRequest, User> {

    @Override
    public User translate(CreateUserRequest createUserRequest) {
        return User.builder()
                    .userName(createUserRequest.getUserName())
                    .password(createUserRequest.getPassword())
                .build();
    }
}
