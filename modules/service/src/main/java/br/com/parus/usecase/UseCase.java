package br.com.parus.usecase;


import br.com.parus.exception.UseCaseException;
import org.reactivestreams.Publisher;


public interface UseCase<T> {

   Publisher<T> execute(T t) throws UseCaseException;

}
