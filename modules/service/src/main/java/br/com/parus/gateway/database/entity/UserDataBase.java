package br.com.parus.gateway.database.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;

@Builder
@Getter
@Setter
@Document(collection = "users")
public class UserDataBase {

      @Id
      private String id;

      private String userName;

      private String password;

      private LocalDate createdAt;

      private LocalDate updatedAt;

}
