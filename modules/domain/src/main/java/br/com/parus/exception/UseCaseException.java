package br.com.parus.exception;

import lombok.Getter;

@Getter
public class UseCaseException extends Exception {

    private final String CODE;

    protected UseCaseException(String message,String code,Throwable ex){
        super(message,ex);
        this.CODE = code;
    }

}
